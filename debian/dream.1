.\"
.\" (C) Copyright 2018 Julian Cable<julian_cable@yahoo.com>,
.\" (C) Copyright 2020 -2022 Garie Miller<gmill777@protonmail.com>,
.\"
.TH DREAM 1 2022-01-07 GNU Dream User Manpage
.\"
.br
.SH NAME 
dream \- Digital Radio Mondiale and analog radio decoder
.PP
.SH SYNOPSIS
.B dream
.RI [ option [argument]] | [input files]
.PP
.SH DESCRIPTION
.br
Dream is a computer based Digital Radio Mondiale (DRM) decoder (thus DReaM). Dream also includes features such as multi mode analog radio support, DRM transmit and Hamlib rig control options, a unique System Evaluation feature providing information about the streams received, automatic update of available DRM stations from the web, plus many others. Dream may be used in combination with a sound card equipped computer or a computer and a Software Defined Radio (SDR).
.PP
DRM is a set of digital radio standards (the primary is ETSI ES 201 980) for the Long, Medium, High, and Very High Frequency ranges. The standards were formed by a consortium in cooperation with the International Telecommunication Union (ITU), and were based in part upon compliant decoding of HE AAC v2 audio streams. Note DRM and Dream now support the newer xHE-AAC codec as well. For more information regarding DRM visit: .UR<www.drm.org>.UE.
.PP
Dream is written mostly in C++. The normal user interface (GUI) is graphical and is built on Qt tools.
.PP
.SH USAGE
.br
For use with a sound card equipped computer, Dream requires a Radio Frequency (RF) front-end with an output Intermediate Frequency (IF) between 5-15 kHz. Any commercial receiver with an IF of 455 kHz should be usable by adding a 455 kHz to 12 kHz adapter (if the receiver bandwidth is sufficient). Purpose built RF front-end's have been used. But SDRs may be the easiest and most commonly used (including modified RTL dongles). Some computer/SDR combinations do not require computer sound card inputs (i.e., those with Universal Serial Bus - USB). See "Receiver_Features" in your Operating System's usr/share/doc/dream directory for more information on using Dream. Also see the SourceForge project pages: .UR<drm.sourceforge.io/wiki/index.php/Main_Page>.UE.
.PP
Some RF front-ends require a computer based control application. For Linux Operating Systems 'Gqrx' is a common choice you may install from many repositories. Note that 'FunCube' may be controlled by Dream using GNU Radio. 
.PP
To listen to short-wave (High Frequency) broadcasts using a modified RTL dongle front-end controlled by Gqrx, typical settings are (assuming you have properly set up for 'direct sample mode'):
.PP
.I "In Gqrx;"
.br
From Configure I/O devices window ( or Menu -> File -> I/O Devices),
.br
   I/Q input, Device -> Generic RTL2832U OEM::00000001,
.br
   Device string -> rtl=0,direct_samp=2,
.br
   Input rate -> 1800000,
.br
   Decimation -> None,
.br
   (Sample rate -> 1.800 Msps)
.br
   Bandwidth -> 0.00 MHz,
.br
   LNBLO -> 0.00 MHz,
.br
   Audio Output, Device -> Built-in Audio Analog Stereo,
.br
   Sample rate -> 48kHz.
.br
From Main Screen window,
.br
   Receiver Options tab (lower right),
.br
   Offset (top) -> 0.0,
.br
   Filter width -> User(10k),
.br
   Filter shape -> Normal,
.br
   Mode -> USB,
.br
   AGC -> Off,
.br
   Squelch -> -150,
.br
   Noise blanker -> off.
.PP
.I "In Dream;"
.br
From DRM Main window,
.br
   Menu -> Settings -> Sound Card -> Signal Input -> Device -> alsa_input [Built-in Audio Analog Stereo],
.br
   Menu -> Settings -> Sound Card -> Signal Input -> Sample Rate -> 48000 Hz,
.br
   Menu -> Settings -> Sound Card -> Audio Output -> Device -> alsa_output [Built-in Audio Analog Stereo],
 .br
   Menu -> Settings -> Sound Card -> Audio Output -> Sample Rate -> 48000 Hz,
.PP
Your settings may vary. For the 'RTL-SDR V.3' most of these settings are the same.
.PP
The main window (first screen) in Dream may say 'Scanning...' This the correct screen for DRM reception. Now tune your desired station (in Gqrx center the pass band). Watch the green vertical bar in the Dream main window near the upper left. Keep it mid-scale by adjusting the audio gain in Gqrx at the lower right. Watch for three green bars under the vertical bar. Listen for audio and notice the station information (if any).
.PP
The Dream license grants you the right to run the program for any purpose. But permission to use Dream to transmit radio signals can ONLY be obtained from the communications authority of your country. If given, this permission will be as stipulated by a commercial or amateur radio license. If you do not have an amateur radio license you may still use Dream for RECEIVING ONLY as a short-wave listener (SWL).
.PP
.SH OPTIONS
.br
Dream may also be run by the optional terminal commands below.
.br
Dream follows GNU command line syntax, with long options starting with two dashes (`-').
.PP
    -h, -?, --help               this help text 
.PP
    -t, --transmitter            DRM transmitter mode
.PP
    -p <b>, --flipspectrum <b>   flip input spectrum (0: off; 1: on)
.PP
    -i <n>, --mlciter <n>        number of MLC iterations (allowed
.br
                                 range: 0...4 default: 1)
.PP
    -s <r>, --sampleoff <r>      sample rate offset initial value [Hz]
.br
                                 (allowed range: -200.0...200.0)
.PP
    -m <b>, --muteaudio <b>      mute audio output (0: off; 1: on)
.PP
    -b <b>, --reverb <b>         audio reverberation on drop-out
.br
                                 (0: off; 1: on)
.PP
    -f <s>, --fileio <s>         disable sound card, use file <s>
.br
                                 instead
.PP
    -w <s>, --writewav <s>       write output to wave file
.PP
    -S <r>, --fracwinsize <r>    freq. acqu. search window size [Hz]
.br
                                 (-1.0: sample rate / 2 (default))
.PP
    -E <r>, --fracwincent <r>    freq. acqu. search window center [Hz]
.br
                                 (-1.0: sample rate / 4 (default))
.PP
    -F <b>, --filter <b>         apply bandpass filter (0: off; 1: on)
.PP
    -D <b>, --modmetric <b>      enable modified metrics (0: off;
.br
                                 1: on)
.PP
    -c <n>, --inchansel <n>      input channel selection
.br
                                   0: left channel;
.br
                                   1: right channel;
.br
                                   2: mix both channels (default);
.br
                                   3: subtract right from left;
.br
                                   4: I / Q input positive;
.br
                                   5: I / Q input negative;
.br
                                   6: I / Q input positive (0 Hz IF);
.br
                                   7: I / Q input negative (0 Hz IF);
.br
                                   8: I / Q input positive split;
.br
                                   9: I / Q input negative split
.PP
    -u <n>, --outchansel <n>     output channel selection
.br
                                   0: L -> L, R -> R (default);
.br
                                   1: L -> L, R muted;
.br
                                   2: L muted, R -> R;
.br
                                   3: mix -> L, R muted;
.br
                                   4: L muted, mix -> R
.PP
    -e <b>, --decodeepg <b>      enable/disable epg decoding (0: off;
.br
                                 1: on)
.PP
    -g <b>, --enablelog <b>      enable/disable logging (0: no logging;
.br
                                 1: logging)
.PP
    -r <n>, --frequency <n>      set frequency [kHz] for log file
.PP
    -l <n>, --logdelay <n>       delay start of logging by <n> seconds,
.br
                                 allowed range: 0...3600)
.PP
    -L <s>, --schedule <s>       read DRMLogger style ini file and obey
.br
                                 it
.PP
    -y <n>, --sysevplotstyle <n> set style for main plot
.br
                                   0: blue-white (default);
.br
                                   1: green-black;
.br
                                   2: black-grey
.PP
    --enablepsd <n>              if 0 then only measure PSD when RSCI
.br
                                 in use otherwise always measure it
.PP
    --mdiout <s>                 MDI out address format [IP#:]IP#:port
.br
                                 (for Content Server)
.PP
    --mdiin  <s>                 MDI in address (for modulator)
.br
                                 [[IP#:]IP:]port
.PP
    --rsioutprofile <s>          MDI/RSCI output profile: A|B|C|D|Q|M
.PP
    --rsiout <s>                 MDI/RSCI output address format
.br
                                 [IP#:]IP#:port (prefix address with
.br
                                 'p' to enable the simple PFT)
.PP
    --rsiin <s>                  MDI/RSCI input address format
.br
                                 [[IP#:]IP#:]port
.PP
    --rciout <s>                 RSCI Control output format IP#:port
.PP
    --rciin <s>                  RSCI Control input address number
.br
                                 format [IP#:]port
.PP
    --rsirecordprofile <s>       RSCI recording profile: A|B|C|D|Q|M
.PP
    --rsirecordtype <s>          RSCI recording file type: raw|ff|pcap
.PP
    --recordiq <b>               enable/disable recording an I/Q file
.PP
    --permissive <b>             enable decoding of bad RSCI frames
.br
                                 (0: off; 1: on)
.PP
    -R <n>, --samplerate <n>     set audio and signal sound card sample
.br
                                 rate [Hz]
.PP
    --audsrate <n>               set audio sound card sample rate [Hz]
.br
                                 (allowed range 8000 - 192000)
.PP
    --sigsrate <n>               set signal sound card sample rate [Hz]
.br
                                 (allowed values 24000, 48000, 96000,
.br
                                 192000)
.PP
    -I <s>, --snddevin <s>       set sound in device
.PP
    -O <s>, --snddevout <s>      set sound out device
.PP
    -U <n>, --sigupratio <n>     set signal upscale ratio
.br
                                 (allowed values: 1, 2)
.PP
    -M <n>, --hamlib-model <n>   set Hamlib radio model ID
.PP
    -C <s>, --hamlib-config <s>  set Hamlib config parameter
.PP
    -T <b>, --ensmeter <b>       enable S-Meter (0: off; 1: on)
.PP
    --test <n>                   if 1 then some test setup will be done
.PP
.SH FILES
.br
If installed from repository or a .deb archive, Dream.ini and DRMSchedule.ini will appear in the user's ~/ directory (i.e., /Home/user/Dream.ini). Folders for other files such as Alternative Frequency Signaling (AFS), Electronic Programme Guide (EPG) and Multimedia Object Transfer (MOT) Broadcast Web Site will appear in ~/data (ie, /Home/user/data/EPG), but may be changed via the Multimedia Settings tab.
.PP
.SH NOTES
.br
The Dream software development was started at:
.I "Darmstadt University of Technology"
Institute of Communication Technology by
.I "Volker Fischer"
and
.I "Alexander Kurpiers"
in 2001-2005. The core digital signal processing and most of the GUI were the result of this early development but more recently development remains active. The Upstream Maintainers of Dream are David Flamand and Julian Cable (for OpenSUSE).
.PP
.SH BUGS
.br
The minimize and expansion buttons (upper right) behave erratically. Dismissing the FM mode or Closing an audio file causes Dream to 'quit' rather than return to the main (DRM) window. The GPS set time function is disabled. Clicking 'Bandpass Filter' causes the System Evaluation window to fall to tray.
.PP
.SH EXAMPLE
.br
dream -p --sampleoff -0.23 -i 2 -r 6140 --rsiout 127.0.0.1:3002
.PP
dream -t
.PP
.SH SEE ALSO
gqrx-sdr(1)
.PP
.SH COPYING
License GPLv2+: GNU GPL version 2 or later.
.br
The Journaline feature of Dream requires a commercial license from Fraunhofer IIS for commercial use. See the included license in the usr/share/doc/dream directory. If you are a non-commercial user of Dream, you may use Journaline without seeking further commercial permission as per GPL2+. Commercial users are also cautioned some libraries linking Dream include commercial clauses.

